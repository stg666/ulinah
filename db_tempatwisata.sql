-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 30 Jul 2018 pada 20.20
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tempatwisata`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_wisata`
--

CREATE TABLE `data_wisata` (
  `kode` int(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `latitude` float(10,6) NOT NULL,
  `langitude` float(10,6) NOT NULL,
  `alamat` text NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `kabupaten` smallint(6) UNSIGNED ZEROFILL NOT NULL,
  `kecamatan` int(8) UNSIGNED NOT NULL,
  `kelurahan` int(10) UNSIGNED NOT NULL,
  `deskripsi` text NOT NULL,
  `harga_dewasa` int(11) DEFAULT NULL,
  `harga_anak` int(11) DEFAULT NULL,
  `hapus` enum('Y','T') NOT NULL DEFAULT 'T',
  `username` varchar(50) DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dinas_pariwisata`
--

CREATE TABLE `dinas_pariwisata` (
  `nip` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `wilayah` enum('Kota Bandung','Kab. Bandung','Kab. Bandung Barat','Kota Cimahi') NOT NULL,
  `hapus` enum('Y','T','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dinas_pariwisata`
--

INSERT INTO `dinas_pariwisata` (`nip`, `nama`, `password`, `wilayah`, `hapus`) VALUES
('10116476', 'Idham', '1234', 'Kab. Bandung Barat', 'T'),
('10116477', 'Fransiskus', '1234', 'Kota Bandung', 'T');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fasilitas`
--

CREATE TABLE `fasilitas` (
  `id` smallint(6) UNSIGNED ZEROFILL NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text,
  `kode` int(10) NOT NULL,
  `hapus` enum('Y','T') NOT NULL DEFAULT 'T'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto_wisata`
--

CREATE TABLE `foto_wisata` (
  `id` smallint(6) UNSIGNED ZEROFILL NOT NULL,
  `nama_file` varchar(255) NOT NULL,
  `hapus` enum('Y','T') NOT NULL DEFAULT 'T',
  `kode` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kabupaten`
--

CREATE TABLE `kabupaten` (
  `kode_kabupaten` smallint(6) UNSIGNED ZEROFILL NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `kode_kecamatan` int(8) UNSIGNED ZEROFILL NOT NULL,
  `kode_kabupaten` smallint(6) UNSIGNED ZEROFILL NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelurahan`
--

CREATE TABLE `kelurahan` (
  `kode_kelurahan` int(10) UNSIGNED ZEROFILL NOT NULL,
  `kode_kecamatan` int(8) UNSIGNED ZEROFILL NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemilik_wisata`
--

CREATE TABLE `pemilik_wisata` (
  `no_ktp` varchar(16) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `alamat` text,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `file_ktp` varchar(255) DEFAULT NULL,
  `hapus` enum('Y','T') NOT NULL DEFAULT 'T',
  `verifikasi` enum('Y','T') NOT NULL DEFAULT 'T',
  `nip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `super_admin`
--

CREATE TABLE `super_admin` (
  `id_super_admin` int(8) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `super_admin`
--

INSERT INTO `super_admin` (`id_super_admin`, `username`, `password`) VALUES
(2, 'superadmin', 'superadmin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `no_ktp` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_wisata`
--
ALTER TABLE `data_wisata`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `username` (`username`),
  ADD KEY `kabupaten` (`kabupaten`),
  ADD KEY `kecamatan` (`kecamatan`),
  ADD KEY `kelurahan` (`kelurahan`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `dinas_pariwisata`
--
ALTER TABLE `dinas_pariwisata`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `fasilitas`
--
ALTER TABLE `fasilitas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode` (`kode`);

--
-- Indexes for table `foto_wisata`
--
ALTER TABLE `foto_wisata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode` (`kode`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`kode_kabupaten`),
  ADD KEY `key` (`nip`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`kode_kecamatan`),
  ADD KEY `KEY` (`kode_kabupaten`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`kode_kelurahan`),
  ADD KEY `KEY` (`kode_kecamatan`);

--
-- Indexes for table `pemilik_wisata`
--
ALTER TABLE `pemilik_wisata`
  ADD PRIMARY KEY (`no_ktp`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `super_admin`
--
ALTER TABLE `super_admin`
  ADD PRIMARY KEY (`id_super_admin`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`),
  ADD KEY `no_ktp` (`no_ktp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_wisata`
--
ALTER TABLE `data_wisata`
  MODIFY `kode` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;
--
-- AUTO_INCREMENT for table `fasilitas`
--
ALTER TABLE `fasilitas`
  MODIFY `id` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=377;
--
-- AUTO_INCREMENT for table `foto_wisata`
--
ALTER TABLE `foto_wisata`
  MODIFY `id` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `kode_kecamatan` int(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3277031;
--
-- AUTO_INCREMENT for table `super_admin`
--
ALTER TABLE `super_admin`
  MODIFY `id_super_admin` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `data_wisata`
--
ALTER TABLE `data_wisata`
  ADD CONSTRAINT `data_kabupaten` FOREIGN KEY (`kabupaten`) REFERENCES `kabupaten` (`kode_kabupaten`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `data_kecamatan` FOREIGN KEY (`kecamatan`) REFERENCES `kecamatan` (`kode_kecamatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `data_kelurahan` FOREIGN KEY (`kelurahan`) REFERENCES `kelurahan` (`kode_kelurahan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `data_nip` FOREIGN KEY (`nip`) REFERENCES `dinas_pariwisata` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `data_wisata_ifbk1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `fasilitas`
--
ALTER TABLE `fasilitas`
  ADD CONSTRAINT `ifbk_fasilitas` FOREIGN KEY (`kode`) REFERENCES `data_wisata` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `foto_wisata`
--
ALTER TABLE `foto_wisata`
  ADD CONSTRAINT `ifbk_foto_wisata` FOREIGN KEY (`kode`) REFERENCES `data_wisata` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD CONSTRAINT `nip_dinas` FOREIGN KEY (`nip`) REFERENCES `dinas_pariwisata` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD CONSTRAINT `kodeKabupaten` FOREIGN KEY (`kode_kabupaten`) REFERENCES `kabupaten` (`kode_kabupaten`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD CONSTRAINT `kodeKecamatan` FOREIGN KEY (`kode_kecamatan`) REFERENCES `kecamatan` (`kode_kecamatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pemilik_wisata`
--
ALTER TABLE `pemilik_wisata`
  ADD CONSTRAINT `nip` FOREIGN KEY (`nip`) REFERENCES `dinas_pariwisata` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`no_ktp`) REFERENCES `pemilik_wisata` (`no_ktp`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
