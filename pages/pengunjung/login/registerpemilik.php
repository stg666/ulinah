<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Register Pemilik</title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">


      <link rel="stylesheet" href="css/style.css">

</head>

<body>

  <div class="form">

        <div id="signup">
          <h1>Register Pemilik</h1>

          <form action="send.php" method="post">

          <div class="top-row">
            <div class="field-wrap">
              <label>
                Nama<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" name="nama"/>
            </div>

            <div class="field-wrap">
              <label>
                No ID KTP<span class="req">*</span>
              </label>
              <input type="text"required autocomplete="off" name="no_ktp"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" name="email"/>
          </div>

          <div class="field-wrap">
            <label>
              Set A Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" name="password"/>
          </div>

		  <div class="field-wrap">
              <label>
                Alamat Lengkap<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" name="alamat"/>
            </div>

			<div class="field-wrap">
              <label>
                Tempat / Tanggal Lahir<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" name="tempat" />
            </div>


          <br>
          <button type="submit" class="button button-block" name="submit" value="Submit"/>Sign Up</button>

          </form>

        </div>

      </div><!-- tab-content -->

</div> <!-- /form -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>



    <script  src="js/index.js"></script>




</body>

</html>
