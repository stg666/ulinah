<!doctype html>
<html lang="en">
  <head>
    <title>Ulin Ah!</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("lib/lib.php") ?>
  </head>
  <body>

    <header class="site-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-4 site-logo" data-aos="fade"><a href="index.php"><em>Ulin Ah!</em></a></div>
          <div class="col-8">
          </div>
          </div>
        </div>
      </div>
    </header>
    <!-- END head -->

    <section class="site-hero overlay" style="background-image: url(img/log_1.jpg)">
      <div class="container">
        <div class="row site-hero-inner justify-content-center align-items-center">
          <div class="col-md-10 text-center">
            <h1 class="heading" data-aos="fade-up"><em> Pilih Login</em></h1>
            <p class="sub-heading mb-5" data-aos="fade-up" data-aos-delay="100">Silahkan pilih login member</p>
            <p data-aos="fade-up" data-aos-delay="100"><a href="login/loginadmin.php" class="btn uppercase btn-outline-light d-sm-inline d-block">Admin</a></p> <br>
            <p data-aos="fade-up" data-aos-delay="100"><a href="login/logindinas.php" class="btn uppercase btn-outline-light d-sm-inline d-block">Dinas</a></p> <br>
            <p data-aos="fade-up" data-aos-delay="100"><a href="login/loginpemilik.php" class="btn uppercase btn-outline-light d-sm-inline d-block">Pemilik</a></p>
          </div>
        </div>
        <!-- <a href="#" class="scroll-down">Scroll Down</a> -->
      </div>
    </section>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/aos.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>
