<?php include("../../log/session-check.php") ?>
<!DOCTYPE html>
<?php
include_once("../../log/koneksi.php");

if(isset($_POST['update']))
{
  $id_tempat_wisata = $_POST['id_tempat_wisata'];
  $id_kelurahan_desa = $_POST['id_kelurahan_desa'];
  $id_kecamatan = $_POST['id_kecamatan'];
  $id_kabupaten_kota = $_POST['id_kabupaten_kota'];
  $id_provinsi = $_POST['id_provinsi'];
  $hapus = $_POST['hapus'];



  // update user data
  $result = mysqli_query($connect, "UPDATE t_detail_tempatwisata SET id_tempat_wisata='$id_tempat_wisata',
  id_kelurahan_desa='$id_kelurahan_desa',
  id_kecamatan='$id_kecamatan',
  id_kabupaten_kota = '$id_kabupaten_kota',
  id_provinsi = '$id_provinsi',
  hapus='$hapus' WHERE id_tempat_wisata='$id_tempat_wisata'");

  // Redirect to homepage to display updated user in list
  header("Location: tampilWisata.php");
}?>
<?php
include_once("../../log/koneksi.php");
// Display selected user data based on id
// Getting id from url
$id_tempat_wisata = $_GET['id_tempat_wisata'];

// Fetech user data based on id
$result = mysqli_query($connect, "SELECT * FROM t_detail_tempatwisata WHERE id_tempat_wisata='$id_tempat_wisata'");

while($row = mysqli_fetch_array($result))
{
  $id_tempat_wisata = $row['id_tempat_wisata'];
  $id_kelurahan_desa = $row['id_kelurahan_desa'];
  $id_kecamatan = $row['id_kecamatan'];
  $id_kabupaten_kota = $row['id_kabupaten_kota'];
  $id_provinsi = $row['id_provinsi'];
  $hapus = $row['hapus'];
}
?>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Admin Dinas</title>
  <?php include("../../lib/lib-dinas.php") ?>
  <!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index.php" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
      </li>
    </ul>



    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->

      <li class="nav-item">
        <a class="nav-link"  href="../../log/session-logout.php"> LOGOUT </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fa fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->


  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="../../dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Welcome</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Admin Dinas</a>
        </div>
      </div>

     <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item has-treeview ">
                    <a href="index.php" class="nav-link active">
                      <i class="nav-icon fa fa-dashboard"></i>
                      <p>
                        Pengolahan Data Wisata
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="tampilWisata.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tampil Data Wisata</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="tambahWisata.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tambah Data Wisata</p>
                        </a>
                      </li>


                    </ul>
                  </li>
 <li class="nav-item has-treeview ">
                    <a href="index.php" class="nav-link active">
                      <i class="nav-icon fa fa-dashboard"></i>
                      <p>
                        Pengolahan Data Wilayah
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="tampilWilayah.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tampil Data Wilayah</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="tambahWilayah.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tambah Data Wilayah</p>
                        </a>
                      </li>


                    </ul>
                  </li>

           <li class="nav-item has-treeview ">
                    <a href="index.php" class="nav-link active">
                      <i class="nav-icon fa fa-dashboard"></i>
                      <p>
                        Pengolahan Akun Pemilik Wisata
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="tampilPemilik.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tampil Data Pemilik WIsata</p>
                        </a>

                    </ul>
                  </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-print"></i>
              <p>
                Print Laporan
              </p>
            </a>
          </li>


      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Home</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>

            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <!-- left column -->
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Ubah Data Wisata</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="post" action="ubahWisata.php" >
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">ID Tempat Wisata</label>
                  <input type="text" class="form-control" placeholder="ID Tempat Wisata" name="id_tempat_wisata" value=<?php echo $id_tempat_wisata;?>>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Kelurahan/Desa</label>
                  <input type="text" class="form-control"  placeholder="Kelurahan/Desa" name="id_kelurahan_desa" value=<?php echo $id_kelurahan_desa;?>>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Kecamatan</label>
                  <input type="text" class="form-control"  placeholder="Kecamatan" name="id_kecamatan" value=<?php echo $id_kecamatan;?>>
                </div>
                <!-- select -->
                <div class="form-group">
                  <label>Kabupaten/Kota</label>
                  <select class="form-control" name="id_kabupaten_kota" value=<?php echo $id_kabupaten_kota;?>>
                    <option>Kota Bandung</option>
                    <option>Kota Cimahi</option>
                    <option>Kab. Bandung Barat</option>
                    <option>Kab. Bandung</option>
                  </select>
                </div>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Provinsi</label>
                  <input type="text" class="form-control"  placeholder="Provinsi" name="id_provinsi" value=<?php echo $id_provinsi;?>>
                </div>
                <!-- select -->
                <div class="form-group">
                  <label>Hapus</label>
                  <select class="form-control" name="hapus" value=<?php echo $hapus;?>>
                    <option value="T">Tidak</option>
                    <option value="Y">Ya</option>
                  </select>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="update">Simpan</button>
              </div>
            </form>


          </div>
          <!-- /.card -->



        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-block-down">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<?php include("../../lib/script-super.php") ?>
<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
