<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title> Admin Dinas</title>
 <?php include("../../lib/lib-dinas.php") ?>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index.php" class="nav-link">Home</a>
      </li>

    </ul>



    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->

    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="../../dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Welcome</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Admin Dinas</a>
        </div>
      </div>

     <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item has-treeview ">
                    <a href="index.php" class="nav-link active">
                      <i class="nav-icon fa fa-dashboard"></i>
                      <p>
                        Pengolahan Data Wisata
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="tampilWisata.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tampil Data Wisata</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="tambahWisata.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tambah Data Wisata</p>
                        </a>
                      </li>


                    </ul>
                  </li>
 <li class="nav-item has-treeview ">
                    <a href="index.php" class="nav-link active">
                      <i class="nav-icon fa fa-dashboard"></i>
                      <p>
                        Pengolahan Data Wilayah
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="tampilWilayah.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tampil Data Wilayah</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="tambahWilayah.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tambah Data Wilayah</p>
                        </a>
                      </li>


                    </ul>
                  </li>

           <li class="nav-item has-treeview ">
                    <a href="index.php" class="nav-link active">
                      <i class="nav-icon fa fa-dashboard"></i>
                      <p>
                        Pengolahan Akun Pemilik Wisata
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="tampilPemilik.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tampil Data Pemilik Wisata</p>
                        </a>

                      <li class="nav-item">
                        <a href="tambahPemilik.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tambah Data Pemilik Wisata</p>
                        </a>
                      </li>
                    </ul>
                  </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-print"></i>
              <p>
                Print Laporan
              </p>
            </a>
          </li>


      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Home</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>

            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tampil Data Pemilik</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <?php
              include("../../log/koneksi.php");

            $sql = 'SELECT no_ktp,nama, email, password, alamat, tempat_lahir,tanggal_lahir, file_ktp, hapus, verifikasi, nip
                FROM pemilik_wisata';

            $query = mysqli_query($connect, $sql);
            if (!$query) {
                  die ('SQL Error: ' . mysqli_error($connect));
                }
              echo '<table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No KTP</th>
                  <th>Nama</th>
                  <th>E-Mail</th>
                  <th>Password</th>
                  <th>Alamat</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>
                  <th>File KTP</th>
                  <th>Hapus</th>
                  <th>verifikasi</th>
                  <th>NIP</th>
                </tr>
                </thead>
                <tbody>';
                  while ($row = mysqli_fetch_array($query))
                {
                  echo '<tr>
                      <td>'.$row['no_ktp'].'</td>
                      <td>'.$row['nama'].'</td>
                      <td>'.$row['email'].'</td>
                      <td>'.$row['password'].'</td>
                      <td>'.$row['alamat'].'</td>
                      <td>'.$row['tempat_lahir'].'</td>
                      <td>'.$row['tanggal_lahir'].'</td>
                      <td>'.$row['file_ktp'].'</td>
                      <td>'.$row['hapus'].'</td>
                      <td>'.$row['verifikasi'].'</td>
                      <td>'.$row['nip'].'</td>
                    </tr>';
                }
              echo '
                </tbody>
              </table>';
            ?>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<?php include("../../lib/script-dinas.php") ?>
</body>
</html>
