<?php include("../../log/session-check.php") ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Dinas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="index.php"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index.php" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
      </li>
    </ul>



    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->

      <li class="nav-item">
        <a class="nav-link"  href="../../log/session-logout.php"> LOGOUT </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fa fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="../../dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin Dinas</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
             <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
               <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview ">
                         <a href="index.php" class="nav-link active">
                           <i class="nav-icon fa fa-dashboard"></i>
                           <p>
                             Olah Tempat Wisata
                             <i class="right fa fa-angle-left"></i>
                           </p>
                         </a>
                         <ul class="nav nav-treeview">
                           <li class="nav-item">
                             <a href="tambahWisata.php" class="nav-link">
                               <i class="fa fa-circle-o nav-icon"></i>
                               <p>Tambah Tempat Wisata</p>
                             </a>
                           </li>
                           <li class="nav-item">
                             <a href="tampilWisata.php" class="nav-link">
                               <i class="fa fa-circle-o nav-icon"></i>
                               <p>Tampil Tempat Wisata</p>
                             </a>
                           </li>


                         </ul>
                       </li>
      <li class="nav-item has-treeview ">
                         <a href="index.php" class="nav-link active">
                           <i class="nav-icon fa fa-dashboard"></i>
                           <p>
                             Olah Data Kecamatan
                             <i class="right fa fa-angle-left"></i>
                           </p>
                         </a>
                         <ul class="nav nav-treeview">
                           <li class="nav-item">
                             <a href="tampilWilayah.php" class="nav-link">
                               <i class="fa fa-circle-o nav-icon"></i>
                               <p>Tambah Data Kecamatan</p>
                             </a>
                           </li>
                           <li class="nav-item">
                             <a href="tampilWilayah.php" class="nav-link">
                               <i class="fa fa-circle-o nav-icon"></i>
                               <p>Tampil Data Kecamatan</p>
                             </a>
                           </li>


                         </ul>
                       </li>

                <li class="nav-item has-treeview ">
                         <a href="index.php" class="nav-link active">
                           <i class="nav-icon fa fa-dashboard"></i>
                           <p>
                             Olah Data Kelurahan
                             <i class="right fa fa-angle-left"></i>
                           </p>
                         </a>
                         <ul class="nav nav-treeview">
                           <li class="nav-item">
                             <a href="tampilPemilik.php" class="nav-link">
                               <i class="fa fa-circle-o nav-icon"></i>
                               <p>Tambah Data Kelurahan</p>
                             </a>
                             <li class="nav-item">
                               <a href="tampilPemilik.php" class="nav-link">
                                 <i class="fa fa-circle-o nav-icon"></i>
                                 <p>Tampil Data Kelurahan</p>
                               </a>
                         </ul>
                       </li>
               <li class="nav-item">
                 <a href="#" class="nav-link">
                   <i class="nav-icon fa fa-print"></i>
                   <p>
                     Print Laporan
                   </p>
                 </a>
               </li>


           <!-- /.sidebar-menu -->
         </div>
         <!-- /.sidebar -->
       </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Home</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-4 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3>Olah</h3>

                  <p>Pengolahan Data Tempat Wisata</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="tampilWisata.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3>Olah</h3>

                  <p>Pengoalah Data Kecamatan</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="tampilSuper.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-4 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3>Olah</h3>

                  <p>Pengoalah Data Desa</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="tampilSuper.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            </div>
            <!-- ./col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.0-alpha
    </div>
    <strong>Copyright &copy; 2014-2018 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
