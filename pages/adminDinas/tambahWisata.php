<?php include("../../log/session-check.php") ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title> Admin Dinas</title>
 <?php include("../../lib/lib-dinas.php") ?>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

</head>
<body class="hold-transition sidebar-mini" onload="initialize()">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index.php" class="nav-link">Home</a>
      </li>

    </ul>



    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->

      <li class="nav-item">
        <a class="nav-link"  href="../../log/session-logout.php"> LOGOUT </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fa fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="../../dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Welcome</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Admin Dinas</a>
        </div>
      </div>

     <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item has-treeview ">
                    <a href="index.php" class="nav-link active">
                      <i class="nav-icon fa fa-dashboard"></i>
                      <p>
                        Pengolahan Data Wisata
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="tampilWisata.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tampil Data Wisata</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="tambahWisata.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tambah Data Wisata</p>
                        </a>
                      </li>


                    </ul>
                  </li>
 <li class="nav-item has-treeview ">
                    <a href="index.php" class="nav-link active">
                      <i class="nav-icon fa fa-dashboard"></i>
                      <p>
                        Pengolahan Data Wilayah
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="tampilWilayah.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tampil Data Wilayah</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="tambahWilayah.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tambah Data Wilayah</p>
                        </a>
                      </li>


                    </ul>
                  </li>

           <li class="nav-item has-treeview ">
                    <a href="index.php" class="nav-link active">
                      <i class="nav-icon fa fa-dashboard"></i>
                      <p>
                        Pengolahan Akun Pemilik Wisata
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="tampilPemilik.php" class="nav-link">
                          <i class="fa fa-circle-o nav-icon"></i>
                          <p>Tampil Data Pemilik WIsata</p>
                        </a>

                    </ul>
                  </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-print"></i>
              <p>
                Print Laporan
              </p>
            </a>
          </li>


      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Home</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>

            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <div class="content">

      <div class="container-fluid">
        <!-- left column -->
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Tambah Data Wisata</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="post">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Tempat Wisata</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Kode Tempat Wisata" name="kode">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Tempat Wisata</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Tempat Wisata" name="nama">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Latitude</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Latitude" name="latitude">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Langitude</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Langitude" name="langitude">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Alamat</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Alamat" name="alamat">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">No Telp</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="No Telp" name="no_tlp">
                </div>
                <!-- select -->
                <div class="form-group">
                  <label>Kabupaten</label>
                  <select class="form-control" name="kabupaten">
                    <option value="Kota Bandung">Kota Bandung</option>
                    <option value="Kota Cimahi">Kota Cimahi</option>
                    <option value="Kab. Bandung Barat">Kab. Bandung Barat</option>
                    <option value="Kab. Bandung">Kab. Bandung</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Kecamatan</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Kecamatan" name="kecamatan">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Kelurahan</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Kelurahan" name="kelurahan">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Deskripsi</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Deskripsi" name="deskripsi">
                  <div class="form-group">
                  <label for="exampleInputPassword1">Harga Dewasa</label>
                  <input type="number" class="form-control" id="exampleInputPassword1" placeholder="Harga Dewasa" name="harga_dewasa">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Harga Anak</label>
                  <input type="number" class="form-control" id="exampleInputPassword1" placeholder="Harga Anak" name="harga_anak">
                </div>
                <!-- select -->
                <div class="form-group">
                  <label>Hapus</label>
                  <select class="form-control" name="hapus">
                    <option value="T">Tidak</option>
                    <option value="Y">Ya</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                  <label for="exampleInputPassword1">Username</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Username" name="username">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">NIP</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="NIP" name="nip">
                </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="tambahWisata">Tambah</button>
              </div>
            </form>
            <?php

            // Check If form submitted, insert form data into users table.
            if(isset($_POST['tambahWisata'])) {
              $kode = $_POST['kode'];
              $nama = $_POST['nama'];
              $latitude = $_POST['latitude'];
              $langitude = $_POST['langitude'];
              $alamat = $_POST['alamat'];
              $no_tlp = $_POST['no_tlp'];
              $kabupaten = $_POST['kabupaten'];
              $kecamatan = $_POST['kecamatan'];
              $kelurahan = $_POST['kelurahan'];
              $deskripsi = $_POST['deskripsi'];
              $harga_dewasa = $_POST['harga_dewasa'];
              $harga_anak = $_POST['harga_anak'];
              $hapus = $_POST['hapus'];
              $username = $_POST['username'];
              $nip = $_POST['nip'];

              // include database connection file
              include_once("../../log/koneksi.php");

              // Insert user data into table
              $result = mysqli_query($connect, "INSERT INTO data_wisata(kode,nama,latitude,langitude,alamat,no_tlp,kabupaten,kecamatan,kelurahan,deskripsi,harga_dewasa,harga_anak,hapus,username,nip)
              VALUES('$kode','$nama','$latitude','$langitude','$alamat','$no_tlp','$kabupaten','$kecamatan','$kelurahan','$deskripsi','$harga_dewasa','$harga_anak','$hapus','$username','$nip')");

              // Show message when user added
              echo "<script type='text/javascript'>alert('Wisata Baru Telah Ditambahkan');</script>";


            }
            ?>
          </div>
          <!-- /.card -->



        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-block-down">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<?php include("../../lib/script-super.php") ?>
<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
